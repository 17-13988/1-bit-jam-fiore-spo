using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlipToObject : MonoBehaviour
{
    GameObject target;

    private void Awake()
    {
        target = GameObject.FindGameObjectWithTag("Player");
    }


    void Update()
    {
        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();

        if (target.transform.position.x < transform.position.x)
        {
            spriteRenderer.flipX = true;
        }

        if (target.transform.position.x > transform.position.x)
        {
            spriteRenderer.flipX = false;
        }

    }
}
