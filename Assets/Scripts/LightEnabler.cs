using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightEnabler : MonoBehaviour
{
    public float value = 10f;

    public AudioSource Flashligh;

    public AudioClip Click;
    public AudioClip Clock;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Flashligh.clip = Click;
            gameObject.GetComponent<FieldOfView>().viewRadius = value;
            Flashligh.Play();
        }
        if (Input.GetMouseButtonUp(0))
        {
            Flashligh.clip = Clock;
            gameObject.GetComponent<FieldOfView>().viewRadius = 0f;
            Flashligh.Play();
        }
    }
}
