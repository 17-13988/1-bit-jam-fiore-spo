using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxDropScript : MonoBehaviour
{
    [SerializeField] Vector3 offset;

    [Header("Audio")]
    public AudioSource audioSource;
    public AudioClip BoxDrop;

    [Header("Particles")]
    public ParticleSystem BoxHitDrop;

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Interactable") || collision.gameObject.CompareTag("Ground") || collision.gameObject.CompareTag("Spikes") )
        {
            audioSource.clip = BoxDrop;
            audioSource.Play();
            var box = collision.gameObject.transform.position - offset;          
            BoxHitDrop.Play();
        }
    }
}
