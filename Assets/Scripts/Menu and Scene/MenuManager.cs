using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour
{
    public GameObject menu;
    bool gamePaused;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!gamePaused)
            {
                PauseGame();
            }
            else if (gamePaused)
            {
                ResumeGame();
            }
        }
    }

    public void PauseGame()
    {
        menu.SetActive(true);
        gamePaused = true;
        Time.timeScale = 0f;
    }
    public void ResumeGame()
    {
        menu.SetActive(false);
        gamePaused = false;
        Time.timeScale = 1f;
    }

}
