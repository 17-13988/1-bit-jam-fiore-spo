using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{

    [SerializeField] string sceneName;
    public void LoadSelectedScene()
    {
        SceneManager.LoadScene(sceneName);
        Time.timeScale = 1f;
    }

    public void LoadThisScene()
    {
        int indiceScena = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(indiceScena);
        Time.timeScale = 1f;
    }

    public void QuitApplication()
    {
        Application.Quit();
    }

}
