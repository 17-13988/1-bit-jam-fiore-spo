using UnityEngine;

public class SpikeScript : MonoBehaviour
{
    [SerializeField] Vector3 offset;

    [Header("Audio")]
    public AudioSource audioSource;

    public AudioClip EnmeyKill;

    [Header("Particles")]
    public GameObject Boom;

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            audioSource.clip = EnmeyKill;
            audioSource.Play();
            Instantiate(Boom, collision.gameObject.transform.position, Quaternion.identity);
            Destroy(collision.gameObject);
            Debug.Log("EnemyDied");
        }
    }
}