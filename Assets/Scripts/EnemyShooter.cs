using UnityEngine;

public class EnemyShooter : MonoBehaviour
{
    [SerializeField] private GameObject bulletPrefab;
    [SerializeField] private float bulletSpeed = 12f;
    [SerializeField] private float shootingInterval = 2f;
    [SerializeField] private float shootingRange = 30f;
    [SerializeField] private Transform bulletSpawnPoint;

    private Transform playerTransform;
    private float shootingTimer = 0f;

    [Header("Audio")]
    public AudioSource audioSource;

    [Header("Animation")]
    public Animator animator;

    private void Start()
    {
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        shootingTimer = shootingInterval;
    }

    private void Update()
    {
        // Calcola la direzione verso il giocatore
        Vector3 direction = playerTransform.position - transform.position;

        // Se il giocatore � nel raggio di tiro, ruota la torretta verso di lui
        if (direction.magnitude <= shootingRange)
        {
            // Se il timer di ripresa � scaduto, spara un proiettile
            shootingTimer += Time.deltaTime;
            if (shootingTimer >= shootingInterval)
            {
                shootingTimer = 0f;
                Shoot(direction.normalized); // Passa la direzione normalizzata al metodo Shoot
            }
        }
    }

    private void Shoot(Vector3 direction)
    {
        // Crea un nuovo proiettile e assegna la velocit� corretta
        GameObject bullet = Instantiate(bulletPrefab, bulletSpawnPoint.position, Quaternion.identity);
        bullet.GetComponent<Rigidbody2D>().velocity = direction * bulletSpeed;
        animator.SetBool("Shoot", true);
        audioSource.Play();
    }

    private void OnDrawGizmos()
    {
        // Imposta il colore dei gizmos per il raggio di azione
        Gizmos.color = Color.red;

        // Disegna il raggio di azione sferico attorno al nemico
        Gizmos.DrawWireSphere(transform.position, shootingRange);
    }

    public void StopShootAnim()
    {
        animator.SetBool("Shoot", false);
    }
}
