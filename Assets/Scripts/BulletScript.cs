using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    [SerializeField] Collider2D col;
    [SerializeField] SpriteRenderer sprite;

    [Header("Audio")]
    public AudioSource audioSource;

    [Header("Particles")]
    public ParticleSystem Boom;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player") || collision.gameObject.CompareTag("Ground") || collision.gameObject.CompareTag("Interactable") || collision.gameObject.CompareTag("Spikes"))
        {
            sprite.enabled = false;
            col.enabled = false;
            audioSource.Play();
            Boom.Play();
            StartCoroutine(DestroyDelay());
        }
    }

    IEnumerator DestroyDelay()
    {
        yield return new WaitForSeconds(0.5f);
        Destroy(gameObject);
    }
}
