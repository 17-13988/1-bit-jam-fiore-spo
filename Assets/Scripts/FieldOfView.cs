using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FieldOfView : MonoBehaviour
{
    private Dictionary<Transform, Color> originalColors = new Dictionary<Transform, Color>();
    private Dictionary<Transform, Rigidbody2DState> originalRigidbody2DStates = new Dictionary<Transform, Rigidbody2DState>();

    public float viewRadius;
    [Range(0, 360)]
    public float viewAngle;

    public LayerMask targetMask;
    public LayerMask obstacleMask;

    public float NewGravityScale = 4;
    public float NewMass = 100;


    [HideInInspector]
    public List<Transform> visibleTargets = new List<Transform>();

    public int edgeResolveIterations;
    public float edgeDstThreshold;

    public MeshFilter viewMeshFilter;
    Mesh viewMesh;

    void Start()
    {
        viewMesh = new Mesh();
        viewMesh.name = "View Mesh";
        viewMeshFilter.mesh = viewMesh;

        StartCoroutine("FindTargetsWithDelay", .2f);
    }

    IEnumerator FindTargetsWithDelay(float delay)
    {
        while (true)
        {
            yield return new WaitForSeconds(delay);
            FindVisibleTargets();
        }
    }

    void Update()
    {
        DrawFieldOfView();
        UpdateDirectionFromMouse();
        UpdateVisibleTargets();
    }

    void UpdateVisibleTargets()
    {
        List<Transform> targetsToRemove = new List<Transform>();

        foreach (Transform target in visibleTargets)
        {
            if (target == null)
            {
                targetsToRemove.Add(target);
                continue;
            }

            Renderer targetRenderer = target.GetComponent<Renderer>();
            Rigidbody2D targetRigidbody2D = target.GetComponent<Rigidbody2D>();

            if (targetRenderer != null)
            {
                if (!originalColors.ContainsKey(target))
                {
                    originalColors.Add(target, targetRenderer.material.color);
                }

                targetRenderer.material.color = Color.black;
            }

            if (targetRigidbody2D != null)
            {
                if (!originalRigidbody2DStates.ContainsKey(target))
                {
                    originalRigidbody2DStates.Add(target, new Rigidbody2DState(targetRigidbody2D.gravityScale, targetRigidbody2D.mass));
                }

                targetRigidbody2D.gravityScale = NewGravityScale;

                if (!targetRigidbody2D.gameObject.CompareTag("Bullet"))
                {
                    targetRigidbody2D.mass = NewMass;
                }
                
            }
        }

        foreach (Transform targetToRemove in targetsToRemove)
        {
            visibleTargets.Remove(targetToRemove);
        }

        List<Transform> nonVisibleTargets = new List<Transform>(originalColors.Keys);
        foreach (Transform target in nonVisibleTargets)
        {
            if (target != null)

            {
                if (!visibleTargets.Contains(target))
                {
                    Renderer targetRenderer = target.GetComponent<Renderer>();
                    Rigidbody2D targetRigidbody2D = target.GetComponent<Rigidbody2D>();

                    if (targetRenderer != null)
                    {
                        targetRenderer.material.color = originalColors[target];
                        originalColors.Remove(target);
                    }

                    if (targetRigidbody2D != null)
                    {
                        if (originalRigidbody2DStates.ContainsKey(target))
                        {
                            Rigidbody2DState state = originalRigidbody2DStates[target];
                            targetRigidbody2D.gravityScale = state.gravityScale;
                            targetRigidbody2D.mass = state.mass;
                            originalRigidbody2DStates.Remove(target);
                        }
                    }
                }
            }
        }
    }

    struct Rigidbody2DState
    {
        public float gravityScale;
        public float mass;

        public Rigidbody2DState(float gravityScale, float mass)
        {
            this.gravityScale = gravityScale;
            this.mass = mass;
        }
    }

    void FindVisibleTargets()
    {
        visibleTargets.Clear();
        Collider2D[] targetsInViewRadius = Physics2D.OverlapCircleAll(transform.position, viewRadius, targetMask);

        for (int i = 0; i < targetsInViewRadius.Length; i++)
        {
            Transform target = targetsInViewRadius[i].transform;
            Vector2 dirToTarget = (target.position - transform.position).normalized;
            if (Vector2.Angle(transform.right, dirToTarget) < viewAngle * 0.5f)
            {
                float dstToTarget = Vector2.Distance(transform.position, target.position);
                if (!Physics2D.Raycast(transform.position, dirToTarget, dstToTarget, obstacleMask))
                {
                    visibleTargets.Add(target);
                }
            }
        }
    }
    void UpdateDirectionFromMouse()
    {
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 directionToMouse = (mousePosition - transform.position).normalized;
        float angleToMouse = Mathf.Atan2(directionToMouse.y, directionToMouse.x) * Mathf.Rad2Deg;
        transform.eulerAngles = new Vector3(0f, 0f, angleToMouse);
    }
    void DrawFieldOfView()
    {
        int stepCount = Mathf.RoundToInt(viewAngle * edgeResolveIterations);
        float stepAngleSize = viewAngle / stepCount;
        List<Vector2> viewPoints = new List<Vector2>();
        ViewCastInfo oldViewCast = new ViewCastInfo();
        for (int i = 0; i <= stepCount; i++)
        {
            float angle = transform.eulerAngles.z - viewAngle / 2 + stepAngleSize * i;
            ViewCastInfo newViewCast = ViewCast(angle);

            if (i > 0)
            {
                bool edgeDstThresholdExceeded = Mathf.Abs(oldViewCast.dst - newViewCast.dst) > edgeDstThreshold;
                if (oldViewCast.hit != newViewCast.hit || (oldViewCast.hit && newViewCast.hit && edgeDstThresholdExceeded))
                {
                    EdgeInfo edge = FindEdge(oldViewCast, newViewCast);
                    if (edge.pointA != Vector2.zero)
                    {
                        viewPoints.Add(edge.pointA);
                    }
                    if (edge.pointB != Vector2.zero)
                    {
                        viewPoints.Add(edge.pointB);
                    }
                }
            }

            viewPoints.Add(newViewCast.point);
            oldViewCast = newViewCast;
        }

        int vertexCount = viewPoints.Count + 1;
        Vector3[] vertices = new Vector3[vertexCount];
        int[] triangles = new int[(vertexCount - 2) * 3];

        vertices[0] = Vector3.zero;
        for (int i = 0; i < vertexCount - 1; i++)
        {
            vertices[i + 1] = transform.InverseTransformPoint(viewPoints[i]);

            if (i < vertexCount - 2)
            {
                triangles[i * 3] = 0;
                triangles[i * 3 + 1] = i + 1;
                triangles[i * 3 + 2] = i + 2;
            }
        }

        viewMesh.Clear();
        viewMesh.vertices = vertices;
        viewMesh.triangles = triangles;
        viewMesh.RecalculateNormals();
    }

    EdgeInfo FindEdge(ViewCastInfo minViewCast, ViewCastInfo maxViewCast)
    {
        float minAngle = minViewCast.angle;
        float maxAngle = maxViewCast.angle;
        Vector2 minPoint = Vector2.zero;
        Vector2 maxPoint = Vector2.zero;

        for (int i = 0; i < edgeResolveIterations; i++)
        {
            float angle = (minAngle + maxAngle) / 2;
            ViewCastInfo newViewCast = ViewCast(angle);

            bool edgeDstThresholdExceeded = Mathf.Abs(minViewCast.dst - newViewCast.dst) > edgeDstThreshold;
            if (newViewCast.hit == minViewCast.hit && !edgeDstThresholdExceeded)
            {
                minAngle = angle;
                minPoint = newViewCast.point;
            }
            else
            {
                maxAngle = angle;
                maxPoint = newViewCast.point;
            }
        }

        return new EdgeInfo(minPoint, maxPoint);
    }

    ViewCastInfo ViewCast(float globalAngle)
    {
        Vector2 dir = DirFromAngle(globalAngle, true);
        RaycastHit2D hit = Physics2D.Raycast(transform.position, dir, viewRadius, obstacleMask);

        if (hit.collider != null)
        {
            return new ViewCastInfo(true, hit.point, hit.distance, globalAngle);
        }
        else
        {
            return new ViewCastInfo(false, (Vector2)transform.position + dir * viewRadius, viewRadius, globalAngle);
        }
    }

    public Vector2 DirFromAngle(float angleInDegrees, bool angleIsGlobal)
    {
        if (!angleIsGlobal)
        {
            angleInDegrees += transform.eulerAngles.z;
        }
        return new Vector2(Mathf.Cos(angleInDegrees * Mathf.Deg2Rad), Mathf.Sin(angleInDegrees * Mathf.Deg2Rad));
    }

    public struct ViewCastInfo
    {
        public bool hit;
        public Vector2 point;
        public float dst;
        public float angle;

        public ViewCastInfo(bool _hit, Vector2 _point, float _dst, float _angle)
        {
            hit = _hit;
            point = _point;
            dst = _dst;
            angle = _angle;
        }
    }

    public struct EdgeInfo
    {
        public Vector2 pointA;
        public Vector2 pointB;

        public EdgeInfo(Vector2 _pointA, Vector2 _pointB)
        {
            pointA = _pointA;
            pointB = _pointB;
        }
    }
}

