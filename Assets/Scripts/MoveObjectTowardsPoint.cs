using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class MoveObjectTowardsPoint : MonoBehaviour
{
    public Transform targetPoint;
    public float movementSpeed = 5.0f;

    private void Update()
    {
        // Calcola la direzione verso il punto di destinazione
        Vector3 direction = targetPoint.position - transform.position;
        direction.Normalize();

        // Calcola la quantit� di movimento basata sulla velocit� e il tempo
        float movementAmount = movementSpeed * Time.deltaTime;

        // Applica il movimento alla posizione dell'oggetto
        transform.position += direction * movementAmount;

        // Controlla se l'oggetto � sufficientemente vicino al punto di destinazione
        float distanceToTarget = Vector3.Distance(transform.position, targetPoint.position);
        if (distanceToTarget < 0.1f)
        {
            // L'oggetto � vicino al punto, puoi eseguire azioni aggiuntive qui se necessario
            Debug.Log("Oggetto ha raggiunto il punto di destinazione.");
        }
    }
}