using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathPlayer : MonoBehaviour
{
    public AudioSource audioSource;
    public AudioClip DeathSound;
    public ParticleSystem DeathParticleSystem;
    

    private void OnCollisionEnter2D(Collision2D collision)
    {
        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
        PlayerSM controller = GetComponent<PlayerSM>();
        Rigidbody2D rigidbody2D = GetComponent<Rigidbody2D>();


        if (collision.gameObject.CompareTag("Bullet") || collision.gameObject.CompareTag("Spikes") || collision.gameObject.CompareTag("Enemy"))
        {
            Debug.Log("Player Died");
            

            DeathParticleSystem.Play();
            audioSource.clip = DeathSound;
            audioSource.Play();
            Debug.Log("deaddd");
            StartCoroutine(WaitBeforeLoading());

            spriteRenderer.enabled = false;
            controller.enabled = false;
            rigidbody2D.velocity = Vector3.zero;
        }
    }

    public void ReloadCurrentScene()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex);
    }

    IEnumerator WaitBeforeLoading()
    {
        yield return new WaitForSeconds(0.5f);
        ReloadCurrentScene();
    }
}
