using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.Events;

public enum State
{
    Grounded, Jumping, Falling,
}

public class PlayerSM : MonoBehaviour
{
    public State currentState = State.Grounded;

    //MOVING VARIABLES
    float horizontalMove = 0f;
    float verticalVelocity = 0f;
    Vector3 velocity;

    public float runSpeed = 30f;
    public float JumpForce = 800f;

    [Range(0, .3f)][SerializeField] float movementSmoothing = 0.07f;

    Rigidbody2D rb;
    SpriteRenderer spriteRenderer;
    [Header("debug checks")]
    [SerializeField] bool isCollidingWithInteractable = false;
    [SerializeField] bool walking;
    [SerializeField] bool jumped;

    [Header("Coyote e buffer")]
    [SerializeField] float jumpBufferTime = 0.2f; // Tempo di buffer per il salto in secondi
    [SerializeField] float coyoteTime = 0.3f; // Tempo di "coyote time" in secondi
    private float jumpPressedTime;
    private float coyoteTimeCounter = 0f;

    [Header("Audio")]
    public AudioSource audioSource;
    public AudioClip JumpSound;
    public AudioClip WalkSound;
    bool wasWalking;

    [Header("Particles")]
    private bool particelleAttive = false;
    public ParticleSystem moveParticles;
    public ParticleSystem landParticles;

    [Header("Animation")]
    public Animator animator;

    private void Start()
    {
       
    }
    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        Flip();
        OnStateUpdate(currentState);

        if (Input.GetButtonDown("Jump"))
        {
            jumpPressedTime = Time.time;
        }
    }

    void Move()
    {
        horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;
        verticalVelocity = rb.velocity.y;
        animator.SetFloat("Speed", Mathf.Abs(horizontalMove));


        Vector3 targetVelocity = new Vector2(horizontalMove, verticalVelocity);
        rb.velocity = Vector3.SmoothDamp(rb.velocity, targetVelocity, ref velocity, movementSmoothing);

        if (Mathf.Abs(horizontalMove) >= 0.1 && !particelleAttive)
        {
            particelleAttive = true;
            wasWalking = true;
            moveParticles.Play();
            audioSource.clip = WalkSound;
            audioSource.pitch = 3;
            audioSource.loop = true;
            audioSource.Play();
        }
        else if (Mathf.Abs(horizontalMove) < 0.1 && particelleAttive)
        {
            particelleAttive = false;
            wasWalking = false;
            moveParticles.Stop();
            audioSource.Stop();
            audioSource.loop = false;
            audioSource.pitch = 1;
        }
    }


    public void ChangeState(State newState)
    {
        if (newState == currentState)
        {
            return;
        }
        
        OnStateExit(currentState);
        currentState = newState;
        OnStateEnter(currentState);
    }

    void OnStateExit(State exitingState)
    {
        switch (exitingState)
        {
            case State.Grounded:
                break;
            
            case State.Jumping:
                break;
            
            case State.Falling:
                break;
        }
    }

    void OnStateEnter(State enteringState)
    {
        switch (enteringState)
        {
            case State.Grounded:

                animator.SetBool("Jump?", false);
                jumped = false;
                landParticles.Play();

                if (wasWalking)
                {
                    audioSource.clip = WalkSound;
                    audioSource.pitch = 3;
                    audioSource.loop = true;
                    audioSource.Play();
                }
                break;
            
            case State.Jumping:
                rb.velocity = new Vector3(0, JumpForce);
                audioSource.pitch = 1;
                audioSource.loop = false;
                jumped = true;
                animator.SetBool("Jump?", true);
                break;
            
            case State.Falling:               
                break;
        }
    }

    void OnStateUpdate(State updatingState)
    {        
        switch (updatingState)
        {
            case State.Grounded:
                Move();

                coyoteTimeCounter = coyoteTime;

                if (Input.GetButtonDown("Jump") || (jumpPressedTime >= 0 && Time.time - jumpPressedTime <= jumpBufferTime))
                {
                    ChangeState(State.Jumping);
                    audioSource.clip = JumpSound;
                    audioSource.Play();
                    return;
                }

                if (verticalVelocity <= -0.1f)
                {
                    ChangeState(State.Falling);
                    return;
                }
                break;
            
            case State.Jumping:

                Move();

                if (verticalVelocity <= -0.1f)
                {
                    ChangeState(State.Falling);
                }
                
                break;

            case State.Falling:
                Move();
                coyoteTimeCounter -= Time.deltaTime;

                if (verticalVelocity > -0.1f)
                {
                    ChangeState(State.Grounded);
                }

                if (Input.GetButtonDown("Jump") && isCollidingWithInteractable)
                {
                    ChangeState(State.Jumping);
                    audioSource.clip = JumpSound;
                    audioSource.Play();
                    return;
                }

               
                if (coyoteTimeCounter > 0 && Input.GetButtonDown("Jump") && !jumped)
                {
                    Debug.Log("Coyote time jump!");
                    ChangeState(State.Jumping);
                    audioSource.clip = JumpSound;
                    audioSource.Play();
                    return;
                }

                break;
        }
    }

    void Flip()
    {
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        if (mousePosition.x < transform.position.x)
        {
            spriteRenderer.flipX = true;
        }

        else if (mousePosition.x > transform.position.x)
        { 
            spriteRenderer.flipX = false;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Interactable"))
        {
            isCollidingWithInteractable = true;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Interactable"))
        {
            isCollidingWithInteractable = false;
        }


    }
}
